/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

/**
 *
 * @author slintas
 */
public class Challenge {

    public static String numberToOrdinal(Integer number) {
        String value = "";
        if (number == 0) {
            return "0";
        }
        Integer temp = number % 100;
        
        //this is more readable
        //logic used is x%10 = 1,2,3,w/e 
        //only numbers in the 10-20 have special cases where instead of 11st it's 11th
        //check if number is between 10-20 if it is it's number + "th"
        //if not check NumMod10=1,2,3 if it is use appropriate ending otherwise "th"
        if((temp < 10) || (temp > 20)){
            switch (number % 10) {
                case 1:
                    value = String.valueOf(number) + "st";
                    break;
                case 2:
                    value = String.valueOf(number) + "nd";
                    break;
                case 3:
                    value = String.valueOf(number) + "rd";
                    break;
                default:
                    value = String.valueOf(number) + "th";
                    break;
            }
        } else {
            value = String.valueOf(number) + "th";
        }
        
        //first itteration of code works fine but not very elegant or readable
        /*
        if (temp > 0) {
            if ((number % 10 == 1) && ((temp < 10) || (temp > 20))) {
                value = String.valueOf(number) + "st";
            } else if ((number % 10 == 2) && ((temp < 10) || (temp > 20))) {
                value = String.valueOf(number) + "nd";
            } else if ((number % 10 == 3) && ((temp < 10) || (temp > 20))) {
                value = String.valueOf(number) + "rd";
            } else {
                value = String.valueOf(number) + "th";
            }
        } else {
            value = "negative";
        }
        /**/
        return value;
    }

}
