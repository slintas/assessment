/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author slintas
 */
public class CreditCard {

    public static String maskify(String creditCardNumber) {
        //not a requirement because of constrains but should be there otherwise
        if((creditCardNumber == null) || (creditCardNumber.length() > 200)){
            return null;
        }
        
        //return String 
        String value = "";
        
        //handles both less than 6 characters and empty strings
        if (creditCardNumber.length() < 6) {
            return creditCardNumber;
        } else {
            //first character of string always shows as is
            value += creditCardNumber.charAt(0);
            //grab all characters except last 4. if they are a digit replace with #
            for (int i = 1; i < creditCardNumber.length() - 4; i++) {
                if (Character.isDigit(creditCardNumber.charAt(i))) {
                    value += "#";
                } else {
                    value += creditCardNumber.charAt(i);
                }
            }
            //last 4 characters show normally regardless of value
            value += creditCardNumber.substring(creditCardNumber.length() - 4);
        }
        return value;
    }
}