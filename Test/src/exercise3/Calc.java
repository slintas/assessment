/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise3;

import java.util.ArrayList;

/**
 *
 * @author slintas
 */
public class Calc {

    public Calc() {
    }

    public double evaluate(String expr) {
        double value = 0;
        //replace useless chars from string
        //if there was a contraint that "'" must be at start and end of string
        //could use if(expr.charAt(0) != '\'') && (expr.charAt(expr.length() -1) != '\'')
        expr = expr.replaceAll("'", "");

        //check if expr is empty
        if (expr.length() == 0) {
            return 0;
        }
        
        //split expresion to tokens
        String[] exp_tokens = expr.split(" ");
        ArrayList<Double> ald = new ArrayList<>();
        
        //flag is checking if there are operators inside the token
        //no_operator simply checks if there was at least one operator in the tokens
        boolean flag,no_operator = true;
        
        for (int i = 0; i < exp_tokens.length; i++) {
            flag = true;
            //check if there was an operator in the token
            //Could have done it with a regular expression and matches
            //but didn't remember the regex out by heart and was asked not to use the internet
            for (int j = 0; j < exp_tokens[i].length(); j++) {
                if ((!Character.isDigit(exp_tokens[i].charAt(j))) && (exp_tokens[i].charAt(j) != '.')) {
                    flag = false;
                }
            }
            if (flag) {
                //digit
                ald.add(Double.valueOf(exp_tokens[i]));
            } else {
                //operator
                no_operator = false;
                //just to avoid writing ald.size() everywhere and 
                int ald_index = ald.size();
                if (exp_tokens[i].length() > 1) {
                    //double operation token. or operation with number attached
                    return -9999;
                }
                
                if(ald.size() < 2){
                    //too many operators or bad order in expr
                    return  -9998;
                }
                
                switch (exp_tokens[i].charAt(0)) {
                    case '+':
                        value = ald.get(ald_index - 2) + ald.get(ald_index - 1);
                        break;
                    case '-':
                        value = ald.get(ald_index - 2) - ald.get(ald_index - 1);
                        break;
                    case '/':
                        value = ald.get(ald_index - 2) / ald.get(ald_index - 1);
                        break;
                    case '*':
                        value = ald.get(ald_index - 2) * ald.get(ald_index - 1);
                        break;
                    default:
                        //unrecognized operation token
                        return -10000;
                }
                //remove last 2 items on list and replace with new value
                ald.remove(ald.size() - 1);
                ald.remove(ald.size() - 1);
                ald.add(value);
            }
        }
        
        if(no_operator){
            return ald.get(ald.size() - 1);
        }

        return value;
    }

}
