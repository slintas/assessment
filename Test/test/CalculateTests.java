/**
 *
 * @author slintas
 */

import exercise3.Calc;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculateTests {

    private Calc calc = new Calc();

    @Test
    public void shouldParseNumbers() {
        assertEquals(3, calc.evaluate("3"), 0);
    }
    @Test
    public void shouldParseFloats() {
        assertEquals(3.5, calc.evaluate("3.5"), 0);
    }
    @Test
    public void shouldSupportAddition() {
        assertEquals(4, calc.evaluate("1 3 +"), 0);
    }
    @Test
    public void shouldSupportMultiplication() {
        assertEquals(3, calc.evaluate("1 3 *"), 0);
    }
    @Test
    public void shouldSupportSubtraction() {
        assertEquals(-2, calc.evaluate("1 3 -"), 0);
    }
    @Test
    public void shouldSupportDivision() {
        assertEquals(2, calc.evaluate("4 2 /"), 0);
    }
    @Test
    public void complex1() {
        assertEquals(3, calc.evaluate("'3 2 1 - *'"), 0);
    }
    @Test
    public void complex2() {
        assertEquals(6, calc.evaluate("'7 4 5 + * 3 - 10 /'"), 0);
    }
    @Test
    public void complex3() {
        assertEquals(6, calc.evaluate("'7 3.5 5.5 + * 3 - 10 /'"), 0);
    }
    @Test
    public void error_tooManyOperators_or_badOrder() {
        assertEquals(-9998, calc.evaluate("'7 3.5 5.5 + * 3 - 10 / +'"), 0);
        assertEquals(-9998, calc.evaluate("'7 + 5'"), 0);
    }
    @Test
    public void error_2operators_numberOperatorAttached() {
        assertEquals(-9999, calc.evaluate("'7 3.5 5.5 + * 3 - 10 /+'"), 0);
        assertEquals(-9999, calc.evaluate("'7 3.5 5.5 + * 3 - 10/'"), 0);
    }
    @Test
    public void error_unrecognized_operator() {
        assertEquals(-10000, calc.evaluate("'7 3.5 5.5 + * 3 - 10 %'"), 0);
    }
    
}
