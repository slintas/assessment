/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import exercise2.Challenge;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author slintas
 */
public class OrdinalTest {
    
    @Test
    public void ordinalTest() {
        assertEquals("1st", Challenge.numberToOrdinal(1));
        assertEquals("2nd", Challenge.numberToOrdinal(2));
        assertEquals("3rd", Challenge.numberToOrdinal(3));
        assertEquals("4th", Challenge.numberToOrdinal(4));
        assertEquals("11th", Challenge.numberToOrdinal(11));
        assertEquals("12th", Challenge.numberToOrdinal(12));
        assertEquals("13th", Challenge.numberToOrdinal(13));
        assertEquals("21st", Challenge.numberToOrdinal(21));
        assertEquals("22nd", Challenge.numberToOrdinal(22));
        assertEquals("23rd", Challenge.numberToOrdinal(23));
        assertEquals("1001st", Challenge.numberToOrdinal(1001));
        assertEquals("1002nd", Challenge.numberToOrdinal(1002));
    }

}
